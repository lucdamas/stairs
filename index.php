<!DOCTYPE html> 
<html>
<head>
	<meta charset="UTF-8">
	<title>Escalier</title>
	<link rel="stylesheet" type="text/css" href="style.css" />
</head>
<body>

	<div id='infos'></div>
	<canvas width="1600" height="900"></canvas>


	<script type='text/javascript' src='main.js'></script>
</body>
</html>

