
// better: http://www.zhitov.ru/en/metal_ladder/

document.addEventListener("DOMContentLoaded", function() {

	let canvas = document.querySelector("canvas");
	let c = canvas.getContext("2d");
	c.lineJoin = "miter";
	c.imageSmoothingEnabled = true;

	let w = canvas.width;
	let h = canvas.height;
	let zoom = 2.4;

	let bottom = 0.0;
	let top = 290.0;
	let levelHeight = 22.0;
	let width = 330.0;
	let levelWidth = 250.0;
	let totalWidth = width+levelWidth;

	let stairHeight = 20.0;
	let stairDepth = 24.0;
	let bottomOffset = 20.0;
	let stairThickness = 4.0;


	let nbRiser = (top-bottom)/stairHeight;
	let step = (width+bottomOffset)/nbRiser;
	draw();

	canvas.addEventListener("wheel", function(event) {
		if (event.deltaY < 0)
			zoom += 0.2;
		else
			zoom -= 0.2;
		draw();
	})

	function draw() {
		c.clearRect(0,0,w,h);
		//============================================VALUES
		let html =  "nb riser: "+nbRiser+ " / "+
					"nb step: "+(nbRiser-1)+ " / "+
					"step: "+(Math.round(step*100)/100)+ " ";
		document.querySelector("#infos").innerHTML = html;

		//=========================================== AXES
		c.setLineDash([3, 10, 40, 10]);
		c.lineWidth = 0.5;

		c.beginPath();
		c.strokeStyle = "#000000";
		c.moveTo(w-levelWidth*zoom,h-top*zoom);
		c.lineTo(w-(levelWidth+width+bottomOffset)*zoom, h-bottom*zoom);
		c.stroke();

		c.beginPath();
		c.moveTo(w-totalWidth*zoom, 0);
		c.lineTo(w-totalWidth*zoom, h);
		c.stroke();

		c.beginPath();
		c.moveTo(w-levelWidth*zoom, 0);
		c.lineTo(w-levelWidth*zoom, h);
		c.stroke();

		c.beginPath();
		c.moveTo(w-(levelWidth+width)*zoom, 0);
		c.lineTo(w-(levelWidth+width)*zoom, h);
		c.stroke();

		c.beginPath();
		c.moveTo(w, 0);
		c.lineTo(w, h);
		c.stroke();
		
		//========================================== LEVELS
		c.setLineDash([]);
		c.lineWidth = 2;

		//----------------------------------------- LEVEL
		c.moveTo(w,h-top*zoom);
		c.lineTo(w-levelWidth*zoom,h-top*zoom);
		c.lineTo(w-levelWidth*zoom, h-(top-levelHeight)*zoom);
		c.lineTo(w, h-(top-levelHeight)*zoom);
		c.stroke();

		//----------------------------------------- BOX
		c.strokeStyle = "#FF9900";
		c.moveTo(w-totalWidth*zoom, h-bottom*zoom);
		c.lineTo(w-(levelWidth+width)*zoom, h-bottom*zoom);
		c.lineTo(w-(levelWidth+width)*zoom, h);
		c.stroke();

		//-------------------------------------------- STAIRS
		for(let i=1; i<nbRiser; i++) {
			c.beginPath();
			let x =  w-(levelWidth)*zoom
				      - (i*step)*zoom;
			let y = h-bottom*zoom - (nbRiser-i)*stairHeight*zoom;
			c.moveTo(x,y);
			c.lineTo(x+stairDepth*zoom, y);
			c.lineTo(x+stairDepth*zoom, y+stairThickness*zoom);
			c.lineTo(x, y+stairThickness*zoom);
			c.lineTo(x,y);
			c.stroke();
		}

		//-------------------------------------------- GIRDER
		



	}

})



















